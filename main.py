from src.calculator import Calculator
from src.operations.sum import SumOperation
from src.operations.sub import SubOperation

calculator = Calculator(sum=SumOperation(), sub=SubOperation())

operation_1 = calculator.addition(5, 11, True)
operation_2 = calculator.subtraction(6, 2, True)

print(f"Operation 1: {operation_1}")
print(f"Operation 2: {operation_2}")