from src.operations.sum import SumOperation
from src.operations.sub import SubOperation

from faker import Faker

fake = Faker()

def test_sum():
  sumOperation = SumOperation()
  
  num_1 = fake.random_number()
  num_2 = fake.random_number()

  expected_sum = num_1 + num_2

  result = sumOperation.sum(num_1, num_2)

  assert result == expected_sum

def test_sub():
  subOperation = SubOperation()
  
  num_1 = fake.random_number()
  num_2 = fake.random_number()

  expected_sub = num_1 - num_2

  result = subOperation.sub(num_1, num_2)

  assert result == expected_sub